package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true){
       

        // Starte spill
            System.out.println("Let's play round " + roundCounter) ;
            
        // Spille
            String human = userChoice();
            String computer = computerChoice();
            String gameString = String.format("Human chose %s, computer chose %s.",human, computer);

        // Hvem vant?
            whoWon(human, computer, gameString);
            
        // Spille på nytt?
            String continuePlaying = continueGame();
            if (continuePlaying.equals("n")) {
                break;
            }
            else {
                roundCounter ++;
            }
        }
        System.out.println("Bye bye :)");



        
        
        
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
   
    
    public boolean validation(String input, List<String> list) { //Validates an input
        return list.contains(input);
    }   
     //Gir true når input er gyldig.    

    public String userChoice() { //Reads the users input, and whether it's valid or not 
        while (true) {
            String human = readInput("Your choice (Rock/Paper/Scissors)?");
            if (validation(human, rpsChoices)){
                return human;}
            else {
                System.out.printf("I don't understand %s. Could you try again?\n", (human));

            }
        }
    }
    
    Random rand = new Random();
    public String computerChoice() {
        int index = rand.nextInt(3);
        String comp = rpsChoices.get(index);
        return comp;
    }
    void whoWon(String p1, String p2, String string) { //Check who won
        if (isWinner(p1, p2)){
            System.out.println(string + " Human wins.");
            humanScore ++;
        }
        else if (isWinner(p2, p1)){
            System.out.println(string + " Computer wins.");
            computerScore ++;
        }
        else {
            System.out.println(string + " It's a tie.");
        }
        System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);
    }

    public boolean isWinner(String p1, String p2) {  //Determine the winner 
        if (p1.equals("paper")) {
            return p2.equals("rock");
        }
        else if (p1.equals("scissors")) {
            return p2.equals("paper");
        }
        else {
            return p2.equals("scissors");
        }

    }
    public String continueGame(){ 
        while (true){
            String input = readInput("Do you wish to continue playing? (y/n)?");
            if (validation(input, Arrays.asList("y", "n"))){
                return input;
            }
            else {
                System.out.printf("I don't understand %s. Could you try again?\n", (input));
            }
        }
    }

}
